// This file is part of the mlhpmkl project. License: See LICENSE

#include "pybind11/pybind11.h"
#include "pybind11/stl.h"

#include "pardiso.hpp"

namespace mlhp::bindings
{


PYBIND11_MODULE( pymlhpmkl, m ) 
{
    m.doc( ) = "Multi-level hp discretization kernel.";

    m.def( "pardisoSolve", &linalg::pardisoSolve );

}

} // mlhp::bindings

