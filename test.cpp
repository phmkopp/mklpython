// This file is part of the 2021_spacetime_am project. License: See LICENSE

#include "mlhp/core.hpp"
#include "pardiso.hpp"

int main( )
{
    using namespace mlhp;

    static constexpr size_t D = 3;

    size_t refinementDepth = 0;
    auto nelements = array::make<size_t, D>( 10 );
    auto lengths = array::make<double, D>( 1.0 );
    size_t polynomialDegree = 2;
    
    double alphaFCM = 1e-3; // needs better preconditioning
    
    auto domain = implicit::invert( implicit::cube<3>( { 0.0,0.101,0.101 }, {1.0, 0.899, 0.899} ) );
    
    auto strategy = refineTowardsDomainBoundary( domain, refinementDepth );
    
    auto grid = makeRefinedGrid( nelements, lengths );
    
    grid->refine( strategy );
    
    size_t nfields = D;
    
    auto basis = makeHpBasis<TensorSpace>( grid, polynomialDegree, nfields );
    
    
    
    SpatialFunctionVector<D> boundaryFunctions( D, spatial::constantFunction<D>( 0.0 ) );
    
    auto boundaryDofs = boundary::boundaryDofs<D>( boundaryFunctions, *basis, { boundary::left } );
    
    
    
    // Constant volume force in z-direction
    std::array<SpatialFunction<D>, D> force 
    {
        spatial::constantFunction<D>( 0.0 ),
        spatial::constantFunction<D>( 0.0 ),
        spatial::constantFunction<D>( 78.5 * 1e3 )
    };
    
    auto E = spatial::constantFunction<D>( 200 * 1e9 );
    auto nu = spatial::constantFunction<D>( 0.3 );
    
    auto integrand = makeLinearElasticityIntegrand( E, nu, force );
    
    
    
    auto matrix = allocateMatrix<linalg::UnsymmetricSparseMatrix>( *basis, boundaryDofs.first );
    std::vector<double> vector( matrix.size1( ), 0.0 );
    
    MomentFittingIntegrationPartitioner<D> partitioner( domain, alphaFCM, polynomialDegree + 1 );
    
    integrateLinearSystemOnDomain( *basis, integrand, matrix, vector, partitioner, boundaryDofs );
    
    auto start = utilities::tic( );
 
    auto interiorSolution = linalg::pardisoSolve( matrix, vector );

    utilities::toc( start, "Solving took: " );
     
    auto solution = boundary::inflate( interiorSolution, boundaryDofs );
    
    
    
    auto domainPostprocessor = spatialFunctionPostprocessor<D>( 
        [&]( std::array<double, D> xyz ){ return domain(xyz); }, "Domain" );
    
    auto solutionProcessor = solutionPostprocessor<D>( solution, nfields, "Displacement" );
    
    postprocess( array::make<size_t, D>( polynomialDegree + 3 ), { solutionProcessor, domainPostprocessor }, *basis, "outputs/linear_elasticity_cpp" );

    // std::cout << "[" << solution[0];
    // for( size_t i = 1; i < solution.size(); ++i )
    //     std::cout << ", " << solution[i];
    // std::cout << "]" << std::endl;

}
