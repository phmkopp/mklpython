import mlhp
import pymlhpmkl
import time

D = 3

print( "1. Setting up mesh and basis" )

refinementDepth = 0
polynomialDegree = 2
nelements = [10] * D
lengths = [1.0] * D

alphaFCM = 1e-3 # needs better preconditioning

domain = mlhp.invert( mlhp.makeImplicitCube( [0.0,0.101,0.101], [1.0, 0.899, 0.899] ) )

strategy = mlhp.refineTowardsBoundary( domain, refinementDepth )

grid = mlhp.makeRefinedGrid( nelements, lengths )
grid.refine( strategy )

basis = mlhp.makeHpTensorSpace( grid, polynomialDegree, nfields=D )

print( "2. Computing dirichlet boundary conditions" )

dirichletFunction = mlhp.makeConstantFunction( D, 0.0 )

dirichlet = mlhp.integrateDirichletDofs( [dirichletFunction]*3, basis, [0] )

print( "3. Setting up physics" )

E = mlhp.makeConstantFunction( D, 200 * 1e9 )
nu = mlhp.makeConstantFunction( D, 0.3 )

rhs = [ mlhp.makeConstantFunction( D, 0.0 ),
        mlhp.makeConstantFunction( D, 0.0 ),
        mlhp.makeConstantFunction( D, 78.5 * 1e3 ) ]

integrand = mlhp.makeLinearElasticityIntegrand( E, nu, rhs )

print( "4. Allocating linear system" )

matrix = mlhp.allocateUnsymmetricSparseMatrix( basis, dirichlet[0] )
vector = mlhp.allocateVectorWithSameSize( matrix )

print( "5. Integrating linear system" )

partitioner = mlhp.makeMomentFittingPartitioner( domain, 
    depth=polynomialDegree + 1, epsilon=alphaFCM )

mlhp.integrateLinearSystemOnDomain( basis, integrand, matrix, 
    vector, dirichletDofs=dirichlet, partitioner=partitioner )

print( "6. Solving linear system" )

start = time.time()

#P = mlhp.makeAdditiveSchwarzPreconditioner( matrix, basis, dirichlet[0] )
#P = mlhp.makeDiagonalPreconditioner( matrix )

#interiorDofs, norms = mlhp.cg( matrix, vector, preconditioner=P, maxit=1000, residualNorms=True )
interiorDofs = mlhp.DoubleVector( pymlhpmkl.pardisoSolve( matrix, vector.get() ) )

end = time.time()

print( "Solving took ", end - start )

allDofs = mlhp.inflateDofs( interiorDofs, dirichlet )

print( "7. Postprocessing solution" )

postprocessSolution = mlhp.makeSolutionPostprocessor( D, allDofs, "Displacement", nfields=D )
postprocessDomain = mlhp.makeImplicitFunctionPostprocessor( domain )

mlhp.postprocess( [polynomialDegree + 3] * D, [ postprocessSolution, postprocessDomain ], 
    basis, "outputs/linear_elasticity" )

# print( allDofs.get( ) )
