#include <mkl_pardiso.h>
#include "mlhp/core.hpp"

namespace mlhp::linalg
{

auto pardisoSolve( const linalg::UnsymmetricSparseMatrix& matrix,
                   const std::vector<double>& rhs )
{
    void* internalSolverMemoryPointer[64];

    MKL_INT iparm[64];
    MKL_INT maximuNumberOfFactorization = 1;
    MKL_INT matrixNumber = 1;
    MKL_INT matrixType = 11;
    MKL_INT numberOfRightHandSides = 1;
    MKL_INT n = static_cast<MKL_INT>( rhs.size( ) );
    MKL_INT error = 0;
    MKL_INT integerDummy = 0;
    MKL_INT messageLevel = 1;

std::cout << "size = " << n << std::endl;
std::cout << "nnz = " << matrix.nnz( ) << std::endl;


    std::fill( internalSolverMemoryPointer, internalSolverMemoryPointer + 64, nullptr );
    std::fill( iparm, iparm + 64, 0 );

    PARDISOINIT( internalSolverMemoryPointer, &matrixType, iparm );

   iparm[1] = 3;  // fill in reducing ordering: 3-> parallel version of metis
    //iparm[9] = 8; // Pivoting perturbation : 10 ^ -8
    iparm[23] = 1; // Parallel factorization control: 1->improved two-level factorization algorithm
    iparm[26] = 1; // Enable matrix consistency checks
    iparm[34] = 1; // One- or zero-based indexing of columns and rows: 1->zero based

    enum class PardisoPhase : MKL_INT
    {
        REORDER = 11, FACTORIZATION = 22, SOLUTION = 33, CLEANUP = -1
    };
    
    auto callPardiso = [&]( PardisoPhase phase, double* data, MKL_INT* indices, 
                            MKL_INT* indptr, double* right, double* solution )
    {
        MKL_INT phaseInt = static_cast<MKL_INT>( phase );

//        std::cout << maximuNumberOfFactorization << ", " << matrixNumber << ", " << matrixType << ", " << phaseInt << ", " << n << ", " << integerDummy << ", " << numberOfRightHandSides << ", " << messageLevel << std::endl;        

        PARDISO_64( internalSolverMemoryPointer, &maximuNumberOfFactorization, &matrixNumber, 
                    &matrixType, &phaseInt, &n, data, indptr, indices, &integerDummy, 
                    &numberOfRightHandSides, iparm, &messageLevel, right, solution, &error );

        if( error != 0 ) std::cout << "Error in pardiso solver (code " << error << ")." << std::endl;
    };

    MLHP_CHECK( -1 == ~0, "Reinterpret cast to signed requires two's complement." );
    MLHP_CHECK( sizeof( linalg::SparsePtr ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );
    MLHP_CHECK( sizeof( linalg::SparseIndex ) == sizeof( MKL_INT ), "Inconsistent integer sizes." );

    // Reinterpret cast to signed works if machine uses two's complement (as tested above).
    auto* i = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indices( ) ) );
    auto* j = const_cast<MKL_INT*>( reinterpret_cast<const MKL_INT*>( matrix.indptr( ) ) );
    auto* v = const_cast<double*>( matrix.data( ) );
    auto* f = const_cast<double*>( rhs.data( ) );

    std::vector<double> solution( rhs.size( ) );

    callPardiso( PardisoPhase::REORDER, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::FACTORIZATION, v, i, j, nullptr, nullptr );
    callPardiso( PardisoPhase::SOLUTION, v, i, j, f, solution.data( ) );
    callPardiso( PardisoPhase::CLEANUP, nullptr, nullptr, nullptr, nullptr, nullptr );

    return solution;
}

} // namespace mlhp::linalg
